import cv2


class VideoCamera(object):
    """Handles all the work done by the camera"""
    def __init__(self):

        self.video = cv2.VideoCapture(0)

    def __del__(self):
        self.video.release()

    def getFrame(self):

        success, frame = self.video.read()

        ret, frameJPEG = cv2.imencode(".jpg", frame)

        return frameJPEG.tobytes()

