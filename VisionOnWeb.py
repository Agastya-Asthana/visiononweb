from flask import Flask, render_template, Response
from Camera import VideoCamera

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

def genCamera(Camera):
    while True:
        frame = Camera.getFrame()
        yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route("/video_feed/")
def video_feed():
    return Response(genCamera(VideoCamera()), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route("/documentationHome/")
def docHome():
    return render_template("start.html")

@app.route("/1259documentation/")
def doc1259():
    return render_template("1259Vision.html")

@app.route("/NormalizeDocumentation/")
def normDoc():
    return render_template("Normalize.html")

"""@app.route("/modules/")
def modules():
    return render_template("modules.html")

@app.route("/genindex/")
def genindex():
    return render_template("genindex.html")"""



if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
