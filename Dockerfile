FROM petronetto/opencv-alpine:latest

RUN apk add --no-cahe python3-dev\
  && pip3 install --upgrade pip

RUN pip3 install wheel
RUN pip3 install flask

WORKDIR /app

COPY . /app

EXPOSE 5000

ENTRYPOINT ["python3"]
CMD ["VisionOnWeb.py"]
